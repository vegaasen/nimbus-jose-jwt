/**
 * JSON Web Key (JWK) classes.
 *
 * <p>This package provides representation, serialisation and parsing of
 * Elliptic Curve (EC), RSA and symmetric JWKs.
 *
 * <p>References:
 *
 * <ul>
 *     <li>http://tools.ietf.org/html/draft-ietf-jose-json-web-key-10
 *     <li>http://tools.ietf.org/wg/jose/
 * </ul>
 *
 * @version $version$ ($version-date$)
 */
package com.nimbusds.jose.jwk;